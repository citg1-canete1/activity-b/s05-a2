<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration form</title>
	<style>
				div {
					margin-top: 5px;
					margin-bottom: 5px;
				}
				div > input,div >select >div >textarea{
					width: 25%;
				}
		</style>

</head>
<body>
	<div id = "containter">
		
		<h1> Welcome to Servlet Job Finder!</h1>
		<form action = "register" method= "post">
			<div> 
			
						<!-- FirstName -->
				<div>
					<label for="firstName">First Name</label>
					<input type="text" name="firstName" required>
				</div>
				
						<!-- LastName -->	
				<div>
					<label for="lastName">Last Name</label>
					<input type="text" name="lastName" required>
				</div>
				
						<!-- Phone no -->
				<div>
					<label for="phoneNo">Phone</label>
					<input type="tel" name="phoneNo" required>
				</div>
				
						<!-- Email -->
				<div>
					<label for="email">Email</label>
					<input type="email" name="email" required>
				</div>
						<!--App discovery  -->
				<fieldset>
					<legend> How did you discover the app? </legend>
						
						<!-- Friends -->
					<input type="radio" name = "appDiscovery" id = "friends" value = "friends" required>
					<label for = "friends" >Friends</label>
					<br>
					
						
						<!-- Social Media -->
					<input type="radio"  name = "appDiscovery" id = "Social Media" value ="SocialMedia" required>
					<label class = "Social Media" >Social Media</label>
					<br>
					
					
						<!-- Other choices -->
					<input type="radio" name = "appDiscovery"  id = "others" name = "others" required>
					<label class = "others">Others</label>
					
				
				</fieldset>
					<!-- Date of Birth -->
				<div>
					<label for="date_of_birth">Date of Birth</label>
					<input type="date" name="date_of_birth" required>
				</div>
				
					<!-- employer or applicant  -->
				<div>
					<label for ="userType" > Are you an employer or applicant?</label>
					<select name ="userType" id = "userType">
						<option value ="" selected ="selected"> Select One</option>
						<option value = "Applicant">Applicant</option>
						<option value = "Employer">Employer</option>
					</select>
				</div>
				
				<div>
					<label for="pdesc">Profile Description</label>
					<textarea name="pdesc" maxlength="500"></textarea>

				</div>
					
					<!-- Button for Submit -->
				<button>Register</button>
			</div>
			
		</form>
	</div>
</body>
</html>